#include <string>
#include <fstream>
#include <iostream>
#include <ctime>

using namespace std;

class Logger
{
private:
	fstream logFile;

public:
	Logger(void)
	{
		logFile.open("card_util.log", ios::app);
		if (!logFile.is_open())
			cerr << "Can't read log" << endl;
	};

	~Logger(void)
	{
		logFile.close();
	};

	void Log(std::string message)
	{
		 time_t now = time(0);
		 tm *localTimeNow = localtime(&now);
		 logFile << message << " - " 
			 << 1 + localTimeNow-> tm_wday
			 << "." << 1 + localTimeNow->tm_mon
			 << "." << 1990+ localTimeNow->tm_year
			 << " - " << 1 + localTimeNow->tm_hour
			 << ":" << 1 + localTimeNow->tm_min
			 << ":" << 1 + localTimeNow->tm_sec 
			 << endl;
	}
};



