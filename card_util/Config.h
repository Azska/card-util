#include <string>
#include <fstream>
#include <iostream>
#include <ctime>
#include "JSON.h"

using namespace std;

class Config
{
private:
	wstring uri;
	wstring login;
	wstring password;
	string wstr2std(wstring str)
	{
		string result(str.begin(), str.end());
		result.assign(str.begin(), str.end());
		return result;
	}
public:
	Config(void)
	{
		ifstream cfgFile("card_util.cfg");
		if( !cfgFile.is_open())
		{
			cerr << "Can't read config" <<endl;
			exit(-1);
			//throw exception("Can't read config" );
		}
		string buffer;
		
		cfgFile.seekg(0, std::ios::end);   
		buffer.reserve(cfgFile.tellg());
		cfgFile.seekg(0, std::ios::beg);

		buffer.assign((std::istreambuf_iterator<char>(cfgFile)),
				   std::istreambuf_iterator<char>());

		JSONValue *value = JSON::Parse(buffer.c_str());
		if( value != NULL)
		{
			JSONObject root;
			if (value->IsObject() != false)
			{
				root = value->AsObject();
				uri = root[L"Server"]->AsString();
				login = root[L"Login"]->AsString();
				password = root[L"Pass"]->AsString();
			}
		}
		cfgFile.close(); 
	}

	~Config(void) {};

	string GetUri()
	{

		return wstr2std(uri);
	}
	string GetLogin()
	{
		return wstr2std(login);;
	}
	string GetPass()
	{
		return wstr2std(password);
	}


};



