#undef UNICODE
#define WIN

#include <iostream>
#include "Logger.h"
#include "Config.h"
#include "curl/curl.h"
#include "JSON.h"
#include <algorithm>

#ifdef WIN
#include <windows.h>
#else
#include  <dlfcn.h>
#endif

using namespace std;

Logger* logger;
Config* configuration;

void parseResult(const char* jsonData);
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);
struct curl_slist* createHeaders(string& login, string& pass);

int main(int argc, char* argv[])
{
	logger = new Logger();

	configuration = new Config();

	string commandName;
	string* commadArgs;


	logger->Log("Start");

	// parse args > command, params
	if (argc==1) {
		logger->Log("Error: No command");
		return -1;
	}
	else
	{
		commandName = argv[1];
		commadArgs = new string[argc-2];
		for(int index = 2; index <argc; index++)
			commadArgs[index-2] = argv[index];
	}

#ifdef WIN
	typedef bool ( _stdcall * f_needPostDataI)(void);
	typedef std::string ( * f_createUriI)( std::string& server, std::string* args);
	typedef std::string (  * f_createPostDataI)(std::string* args);
	f_needPostDataI needPostData;
	f_createUriI createUri;
	f_createPostDataI createPostData;

	//loading
	HINSTANCE hModule=NULL;

	string moduleName = commandName + ".dll";
	hModule=LoadLibrary((LPCSTR)moduleName.c_str());

	if (hModule!=NULL)
	{
		needPostData =  (f_needPostDataI)GetProcAddress((HMODULE)hModule, "needPostData");
		createUri =  (f_createUriI)GetProcAddress((HMODULE)hModule, "createUri");
		createPostData =  (f_createPostDataI)GetProcAddress((HMODULE)hModule, "createPostData");

		if ( needPostData == NULL)
		{
			logger->Log("ERROR: can't load funtion \"needPostData\"");
			cerr << "ERROR: can't load funtion \"needPostData\"" << endl;
			exit(-1);
		}
		if ( createUri == NULL)
		{
			logger->Log("ERROR: can't load funtion \"createUri\"");
			cerr << "ERROR: can't load funtion \"createUri\"" << endl;
			exit(-1);
		}
	}
	else 
	{
		logger->Log("ERROR: can't load dll" );
		cerr << "ERROR: can't load dll" << endl;
		exit(-1);
	}
#else
	void *module;
	bool (needPostData)(void);
	string (createUri)(string& server,string* commandArgs);
	string (createPostData)(string* commandArgs);

	string moduleName = commandName + ".so";
	module = dlopen(moduleName,RTLD_LAZY);
	if (module)
	{
		createUri = dlsym(module, "createUri" );
		needPostData = dlsym(module, "needPostData" );
		createPostData =dlsym(module, "createPostData");

		if ( needPostData == NULL)
		{
			logger->Log("ERROR: can't load funtion \"needPostData\"");
			cerr << "ERROR: can't load funtion \"needPostData\"" << endl;
			exit(-1);
		}
		if ( createUri == NULL)
		{
			logger->Log("ERROR: can't load funtion \"createUri\"");
			cerr << "ERROR: can't load funtion \"createUri\"" << endl;
			exit(-1);
		}
		if ( createPostData == NULL)
		{
			logger->Log("ERROR: can't load funtion \"createPostData\"");
			cerr << "ERROR: can't load funtion \"createPostData\"" << endl;
			exit(-1);
		}
	}
	else 
	{
		logger->Log("ERROR: can't load dll" );
		cerr << "ERROR: can't load dll" << endl;
		exit(-1);
	}
#endif

	CURL *curl;
	CURLcode res;
	string readBuffer;
	curl = curl_easy_init();
	if(curl) {

		//create headers
		struct curl_slist *chunk = NULL;
		chunk = createHeaders( configuration->GetLogin(), configuration->GetPass());

		res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		//create url
		string tmp = createUri( configuration->GetUri(), commadArgs);
		curl_easy_setopt(curl, CURLOPT_URL, tmp.c_str());

		if( needPostData() )
		{
			// ok, now MUST to check
			if ( createPostData == NULL)
			{
				logger->Log("ERROR: can't load funtion \"createPostData\"");
				cerr << "ERROR: can't load funtion \"createPostData\"" << endl;
				exit(-1);
			}
			string postData = createPostData(commadArgs);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData.c_str());
		}

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK) {
			string str(curl_easy_strerror(res));
			cerr << "Failed: " << curl_easy_strerror(res) << endl;
			logger->Log( " ERROR: curl error. " + str);
			exit(-1);
		}
		else
		{
			logger->Log("SUCSESS: " + commandName + "executed");
		}
	}
	curl_easy_cleanup(curl);

	parseResult(readBuffer.c_str());

	delete[] commadArgs;
#ifdef WIN
	if (hModule != NULL)
		FreeLibrary(hModule);
#else
	dlclose(module);
#endif
	return 0;
}

// ������� ������ �������
void parseResult(const char* jsonData)
{
	JSONValue *value = JSON::Parse(jsonData);
	if( value != NULL)
	{
		JSONObject root;
		if (value->IsObject() != false)
		{
			cout << "response status: 0" << endl;
			cout << "response_message: sucsess"<< endl;

			root = value->AsObject();
			for( JSONObject::iterator i = root.begin(); i !=root.end(); ++i)
			{
				// key
				wstring tmp;
				tmp = (i->first);
				string s(tmp.begin(), tmp.end());
				s.assign(tmp.begin(), tmp.end());
				cout << s << ":";
				// value
				if ( (i->second)->IsString())
				{
					tmp = (i->second)->AsString();
					string s(tmp.begin(), tmp.end());
					s.assign(tmp.begin(), tmp.end());
					cout << s << endl;
				}
				else if ( (i->second)->IsNumber())
				{
					cout  <<  (i->second)->AsNumber() << endl;
				}
			}
		}
	}
	else
	{
		cout << "response status: 1" << endl;
		cout << "response_message: not JSON" << endl;
	}

}

// call-back ��� ������ ���������� �������
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

struct curl_slist* createHeaders(string& login, string& pass)
{
	struct curl_slist *chunk = NULL;
	chunk = curl_slist_append(chunk, "Content-Type : application/json");
	char data[255];
	strcpy(data, "Authorization:");
	strcat(data, login.c_str());
	strcat(data, " ");
	strcat(data, pass.c_str());
	chunk = curl_slist_append(chunk, data);

	return chunk;
};
