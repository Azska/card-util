#include <string>
#include <iostream>
#include "curl\curl.h"

using namespace std;


extern "C"  __declspec(dllexport) bool needPostData(){ 
	return true;
};

extern "C"  __declspec(dllexport) string createUri(string& server,string* commandArgs)
{
	char str[255];
	strcpy(str, server.c_str());
	strcat(str, "v1.0/cards/");
	strcat(str, commandArgs[0].c_str());
	strcat(str, "/transactions");

	string s(str);

	return s;
};

extern "C"  __declspec(dllexport) string createPostData(string* commandArgs)
{
		char post[500];
		strcpy(post," { \"sum\":");
		strcat(post, commandArgs[1].c_str());
		strcat(post, " \"type\": \"spent\" }");
		string str(post); 
		return str;
};